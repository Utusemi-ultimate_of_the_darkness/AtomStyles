# AtomStyles
<div>
    github製テキストエディタ「Atom」のスタイルシートです．<br>
    「カスタムメイド」ならぬ「カスタムアトム」作成のご参考になればと思います．<br>
    Windows非対応．Mac使用者で，ダークテーマや暗い背景画像を使用している方，「カスタムメイド」が通じる方にオススメ．<br>
    もし，「あ？動かねーぞ？ぶっ飛ばすぞfu**がぁ！」などあれば日本語で具体的にご指摘いただけると幸いです．<br>
</div>


<h3>設定方法とか</h3>

<p>japanese-menuインストール済が前提</p>

<ol>
    <li>
        お好きなバージョンをダウンロード
    </li>
    <li>
        ダウンロードしたフォルダの中の「AtomStyles.less」をテキストエディタ(Atomとかメモ帳など)で開く
    </li>
    <li>
        Atomを起動する
    </li>
    <li>
        使用中のPCがMacの場合は，画面の一番左上のリンゴマークのその右隣，「Atom」>「スタイルシート...」をクリック．<br>
        Windowsの場合は「ファイル」>「スタイルシートを開く」をクリック．
    </li>
    <li>
        先ほどダウンロードして開いておいたスタイルシートを全選択しコピー．Atomのスタイルシートに貼り付け，編集，保存．おわり．
    </li>
</ol>


<h3>できること</h3>

<ul>
    <li>
        ペインの枠線色の変更
    </li>
    <li>
        タブの背景色，文字色，文字フォントの変更
    </li>
    <li>
        アクティブタブの背景色，文字の大きさ，フォントの変更，タブの下線の色，太さの変更
    </li>
    <li>
        ツリービュー(プロジェクトフォルダ表示欄)
        <ul>
            <li>
                背景色，文字色，文字フォントの変更
            </li>
        </ul>
    </li>
    <li>
        作業スペースの背景色，背景画像，文字色の変更
    </li>
    <li>
        行番号部分
        <ul>
            <li>
                行番号部分とデバッガ間の境界線の色の変更
            </li>
            <li>
                カーソルがある行番号の背景色，文字色の変更
            </li>
            <li>
                テキストを選択している状態での背景色，文字色の変更
            </li>
        </ul>
    </li>
    <li>
        テキスト選択部分の背景色の変更
    </li>
    <li>
        "highlight-selected"プラグイン
        <ul>
            <li>
                同文字列の枠線，背景色の変更
            </li>
        </ul>
    </li>
    <li>
        "show ideographic space"(全角スペース表示プラグイン)
        <ul>
            <li>
                全角スペースの背景色，文字色，表示する文字の変更
            </li>
        </ul>
    </li>
    <li>
        文字列検索で一致した文字列の枠線の色の変更
    </li>
    <li>
        カーソルのある行の水平線の色，太さの変更
    </li>
    <li>
        カーソルの色の変更
    </li>
    <li>
        コメントの色の変更
    </li>
    <li>
        "highlight-column"(入力桁の垂直線を表示する)プラグイン
        <ul>
            <li>
                背景色の変更
            </li>
        </ul>
    </li>
    <li>
        "browser-plus"(ブラウザ追加)プラグイン
        <ul>
            <li>
                ナビゲーションバー境界線の変更
            </li>
            <li>
                各ボタンの境界線の色の変更
            </li>
            <li>
                有効ボタンの色の変更
            </li>
            <li>
                アドレスバーの背景色，文字色，文字フォントの変更
            </li>
        </ul>
    </li>
    <li>
        ツールパネル
        <ul>
            <li>
                "platformio-ide-terminal"(ターミナル追加)プラグイン
                <ul>
                    <li>
                        枠線，境界線，背景画像の設定
                    </li>
                    <li>
                        ボタンツールバーのボタンの枠線の色，太さ，ボタンの背景色，ボタンアイコンの色の変更
                    </li>
                    <li>
                        ターミナル部分の背景，背景色，文字色，文字フォントの変更
                    </li>
                    <li>
                        選択部分の背景色，文字色の変更
                    </li>
                    <li>
                        カーソルの形，太さ，色の変更
                    </li>
                </ul>
            </li>
            <li>
                文字列検索，プロジェクト内検索
                <ul>
                    <li>
                        境界線，背景，文字色の変更
                    </li>
                    <li>
                        ボタン枠線，背景，文字色の変更
                    </li>
                </ul>
            </li>
        </ul>
    </li>
    <li>
        ドック
        <ul>
            <li>
                開閉ボタンの背景色，文字色の変更
            </li>
            <li>
                ドック背景画像の設定
            </li>
        </ul>
    </li>
    <li>
        ステータスバー
        <ul>
            <li>
                背景色，文字色，文字フォント，フォントサイズの変更
            </li>
            <li>
                各アイコンの背景，文字色の変更
            </li>
            <li>
                一部要素の文字フォント変更
            </li>
        </ul>
    </li>
</ul>



<h3>バージョン情報</h3>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_1.0.0">ver_1.0.0</a></h4>
<ul>
    <li>
        とりあえず書いていたものの最適化と整理
    </li>
    <li>
        コメント等の追記や修正
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_1.0.1">ver_1.0.1</a></h4>
<ul>
    <li>
        コメント文の整理と追記
    </li>
    <li>
        ターミナル追加パッケージの背景と文字サイズの変更
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_1.0.2">ver_1.0.2</a></h4>
<ul>
    <li>
        背景透明度の変更
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_1.0.3">ver_1.0.3</a></h4>
<ul>
    <li>
        コメント文の整理と追記
    </li>
    <li>
        文字フォントの追加(日本語文字フォントの設定)
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_1.0.4">ver_1.0.4</a></h4>
<ul>
    <li>
        背景色と透過率の変更
    </li>
    <li>
        タブの背景透過と文字サイズの変更
    </li>
    <li>
        "highlight-selected"プラグインの枠線，背景の変更
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_2.0.0">ver_2.0.0</a></h4>
<ul>
    <li>
        スタイル指定方法の変更
        <ul>
            <li>
                親要素，子要素の関係の明確化
            </li>
            <li>
                より細部の特定の部分のみにスタイル適用可能
            </li>
        </ul>
    </li>
    <li>
        各ペイン要素の境界線表示
    </li>
    <li>
        プロジェクトフォルダのツリービューの文字色変更
    </li>
    <li>
        プロジェクトフォルダ表示欄の開閉ボタンの背景色，文字色の変更
    </li>
    <li>
        アクティブタブの背景色，文字の太さ変更
    </li>
    <li>
        アクティブタブに下線を表示
    </li>
    <li>
        行番号部分とブックマーク表示部分の境界線を表示
    </li>
    <li>
        選択中のテキストの背景色変更
    </li>
    <li>
        入力中の桁(縦ライン)の背景色変更
    </li>
    <li>
        "browser plus"(ブラウザーを追加するプラグイン)
        <ul>
            <li>
                ブラウザのナビゲーションバーに枠線と境界線を表示
            </li>
            <li>
                ブラウザのナビゲーションバーの有効なボタンの色を変更
            </li>
            <li>
                アドレスバーの背景色と文字色の変更
            </li>
        </ul>
    </li>
    <li>
        "platformio-ide-terminal"プラグインのツールバーとターミナル部分の境界線の表示
    </li>
    <li>
        Linter開閉ボタンの背景色文字色変更
    </li>
    <li>
        ステータスバーのアイコン類の文字色変更
    </li>
    <li>
        コメントアウトしてあった不要な部分の削除
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_2.0.1">ver_2.0.1</a></h4>
<ul>
    <li>
        タブの背景を完全透明化
    </li>
    <li>
        アクティブタブのフォント，アンダーラインの太さ変更
    </li>
    <li>
        "highlight-selected"プラグインの背景色変更
    </li>
    <li>
        マッチング検索結果の枠線の色と形を変更
    </li>
    <li>
        コメントの追記
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_2.1.0">ver_2.1.0</a></h4>
マージ不能のため，新ブランチ作成

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_2.1.1">ver_2.1.1</a></h4>
<ul>
    <li>
        設定画面
        <ul>
            <li>
                背景画像，背景色の変更
            </li>
            <li>
                文字色，文字フォントの変更
            </li>
            <li>
                各要素の枠線，背景色，文字色の色の変更
            </li>
        </ul>
    </li>
    <li>
        ツールパネル
        <ul>
            <li>
                背景色の変更
            </li>
            <li>
                文字色，文字フォントの変更
            </li>
            <li>
                各要素の枠線，背景色，文字色の変更
            </li>
        </ul>
    </li>
    <li>
        ステータスバー左下のファイル名表示欄の最大幅変更
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_2.1.2">ver_2.1.2</a></h4>
<ul>
    <li>
        変数類などの整理
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_2.1.3">ver_2.1.3</a></h4>
<ul>
    <li>
        コメントの追記，整理
    </li>
    <li>
        主要な要素の色の変数化
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_2.1.4">ver_2.1.4</a></h4>
<ul>
    <li>
        ツールパネル一部文字色変更
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_2.1.5">ver_2.1.5</a></h4>
<ul>
    <li>
        ツリービューのタブバーの背景色変更
    </li>
    <li>
        ツリービューのタブの背景色変更
    </li>
    <li>
        ツリービューのアクティブタブにアンダーライン表示
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_2.2.0">ver_2.2.0</a></h4>
<ul>
    <li>
        platformioの導入
    </li>
    <li>
        platformioツールパネルの枠線や背景色，文字色を変更
    </li>
    <li>
        terminalのボタンの枠線色を変更
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_2.2.1">ver_2.2.1</a></h4>
<ul>
    <li>
        platformioツールパネルのボタンアイコンのサイズの変更
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_3.0.0">ver_3.0.0</a></h4>
<ul>
    <li>
        「One Dark」テーマに対応
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_3.0.1">ver_3.0.1</a></h4>
<ul>
    <li>
        pratformio-ide-terminalで最下行のはみ出しを改善
    </li>
    <li>
        設定画面サブセクションタイトル背景透過
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_3.0.2">ver_3.0.2</a></h4>
<ul>
    <li>
        コメントの追記
    </li>
    <li>
        コードの可読性の改善
    </li>
</ul>


<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_3.0.3">ver_3.0.3</a></h4>
<ul>
    <li>
        設定画面で一部要素にスタイルが適用できていなかった部分の改善
    </li>
    <li>
        ボタンのアイコン位置の修正
    </li>
    <li>
        フッタアイコン色の修正
    </li>
    <li>
        不要部分の削除
    </li>
    <li>
        コメントの修正
    </li>
</ul>


<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/v4.0.0_OneDark">v4.0.0_OneDark</a></h4>
<ul>
    <li>
        テーマ別スタイルの「One Dark」用スタイル．
    </li>
    <li>
        各ペイン，ドックでの背景画像を指定可能
    </li>
    <li>
        各要素別に文字フォントの指定
    </li>
    <li>
        設定画面で一部要素にスタイルが適用できていなかった部分の修正
    </li>
    <li>
        ステータスバーのアイコン背景色変更
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_4.1.0">ver_4.1.0</a></h4>
<ul>
    <li>
        Atom Darkテーマに対応
    </li>
    <li>
        One Darkテーマに対応
    </li>
    <li>
        テキスト選択時に行番号が重なる問題の解消
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_4.1.0_AtomDark">v4.1.0 AtomDark</a></h4>
<ul>
    <li>
        Atom Darkテーマに対応
    </li>
    <li>
        テキスト選択時に行番号が重なる問題の解消
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/AtomStyles/tree/ver_4.1.0_OneDark">v4.1.0 OneDark</a></h4>
<ul>
    <li>
        One Darkテーマに対応
    </li>
    <li>
        テキスト選択時に行番号が重なる問題の解消
    </li>
</ul>
